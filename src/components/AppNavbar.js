import { useContext } from 'react';
import { Link, NavLink } from 'react-router-dom';

import UserContext from '../UserContext';

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

export default function AppNavbar() {
	const { user, unsetUser } = useContext(UserContext);

	const logout = () => {
		unsetUser();
	}

	const rightNav = (user.id === null)?(
		<>
			<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
			<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
		</>
	):(
		<>
			<Nav.Link onClick={logout}>Logout</Nav.Link>
		</>
	);

	return (
	    <Navbar bg="light" expand="lg">
			<Navbar.Brand as={Link} to ="/">Divi Magic</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav"/>
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="mr-auto">
					<Nav.Link as={NavLink} to="/">Home</Nav.Link>
					<Nav.Link as={NavLink} to="/courses">Products</Nav.Link> 
					<Nav.Link as={NavLink} to="/courses">Cart</Nav.Link> 
					<Nav.Link as={NavLink} to="/courses">Profile</Nav.Link>   
					<Nav.Link as={NavLink} to="/courses">Contact Us</Nav.Link>        
				</Nav>
				<Nav className="ml-auto">
					{rightNav}
				</Nav>
			</Navbar.Collapse>
	    </Navbar>		
	);
}