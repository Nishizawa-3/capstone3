import Jumbotron from 'react-bootstrap/Jumbotron';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

export default function Banner() {
	return (
		<Row>
			<Col>
				<Jumbotron className="text-center">
					<h1>Magic Studio</h1>
					<p>Where magic is delivered from</p>
					<Button>Learn More!</Button>
				</Jumbotron>
			</Col>
		</Row>
	)
}