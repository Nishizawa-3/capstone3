import { useState } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

import UserContext from './UserContext';

import AppNavbar from './components/AppNavbar';

import Home from './pages/Home';
import Login from './pages/Login';
// import NotFound from './pages/NotFound';
import Register from './pages/Register';


export default function App() {
    //Creating a user state from app component provides a sigle source of truth
    const [user, setUser] = useState({ 
        token: localStorage.getItem('token')
    });

    //Function to be used for logging out
    const unsetUser = () => {
        localStorage.clear();
        setUser({ token: null});
    }

    return (
        <UserContext.Provider value={{ user, setUser, unsetUser }}>
            <BrowserRouter>
                <AppNavbar/>
                <Routes>
                    <Route exact path="/" element={<Home/>}/>
                    <Route exact path="/login" element={<Login/>}/>
                    <Route exact path="/register" element={<Register/>}/>
{/*                    <Route path="*" element={<NotFound/>}/>*/}
                </Routes>
            </BrowserRouter>
        </UserContext.Provider>
    );
}